<?php

namespace App\Controller;

use App\Entity\Post;
use App\Entity\User;
use App\Form\PostType;
use App\Repository\PostRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use function Sodium\add;

class PostController extends AbstractController
{
    /**
     * @Route("/post", name="post")
     * @param Request $request
     * @param AuthenticationUtils $authenticationUtils
     * @param $offset
     * @return Response
     */
    public function list(Request $request, AuthenticationUtils $authenticationUtils): Response
    {
        /**
         * @var PostRepository $repo
         */

        $repo = $this->getDoctrine()->getRepository(Post::class);
        $posts = $repo->findAll();
        $recentPosts = $repo->findRecentPosts(3);

        $pagenumber = $request->query->get('number');

//        $limit = 5;
        $parents = $repo->findBy([
            'parent'=>null
        ]);


        //creation formulaire
        // On instancie un nouveau post
        $post = new Post();

        // création du formulaire pour l'instance
        $form = $this->createForm(PostType::class, $post);

        // on définit l'author comme étant l'utilisateur actuel connecté
        $user = $this->getUser();
        $post->setAuthor($user);

        // Cette fonction sert à réccupérer la requête lorsqu'on valide le formulaire, il faut donc l'associer avec le param de la fonction create plus haut.
        $form->handleRequest($request);


        // si le formulaire a été soumis on valide
        if ($form->isSubmitted()) {
            // si les donnes sont valides,
            if ($form->isValid()) {
                //on reccup les donnes
                $post = $form->getData();


                // on enregistre en BDD
                // 1. On reccup le Manager Doctrine
                $manager = $this->getDoctrine()->getManager();
                // 2. On dit au Manager qu'il doit "gérer/surveiller" une nouvelle instance d'une Entité
                $manager->persist($post);
                // 3. On dit au Manager de mettre la BDD à jour
                $manager->flush();

                // si c'est OK, redirige
                return $this->redirectToRoute('post');
            }
        }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        // ici, on définit la vue sur laquelle on veut aller ainsi que la variable twig, que l'on associe à la variable PHP avec le tableau
        return $this->render('post/postlist.html.twig', [
            'posts' => $posts,
            "recentPosts"=>$recentPosts,
            "postForm"=>$form->createView(),
            "error"=> $error,
            "parents"=>$parents,
        ]);
    }

    /**
     * @Route("/userprofil/{user}", name="userprofil")
     * @param $user
     * @return Response
     */
    public function userProfil($user): Response
    {

        $userPost = $this->getDoctrine()->getRepository(User::class)->findOneBy([
            "username" => $user
        ]);

        if (empty($userPost)){
            throw $this->createNotFoundException("utilisateur inexsitant");
        }

        // On réccup les followers pour pouvoir les afficher dans twig
        $getFollows = $userPost->getFollow();
        $peopleIFollow = $userPost->getFollowers();

        $getFriends = $userPost->getFriend();
        $peopleIaccept = $userPost->getFriends();

        $parents = $this->getDoctrine()->getRepository(Post::class)->findBy([
            'parent'=>null,
            'author'=>$userPost
        ]);

        $posts = $this->getDoctrine()->getRepository(Post::class)->findBy(
            ['author'=>$userPost]
        );

        return $this->render('/userprofil.html.twig', [
            'user'=> $userPost,
            'posts'=> $posts,
            'parents'=>$parents,
            'getFollowers'=>$getFollows,
            'peopleIFollow'=>$peopleIFollow,
            'getFriends'=>$getFriends,
            'peopleIaccept'=>$peopleIaccept
//            'id'=>$id
        ]);
    }

    /**
     * @Route ("/formulaire", name="formulaire")
     */
    public function create(Request $request) {
        // On instancie un nouveau post
        $post = new Post();

        // création du formulaire pour l'instance
        $form = $this->createForm(PostType::class, $post);

        // on définit l'author comme étant l'utilisateur actuel connecté
        $user = $this->getUser();
        $post->setAuthor($user);

        // Cette fonction sert à réccupérer la requête lorsqu'on valide le formulaire, il faut donc l'associer avec le param de la fonction create plus haut.
        $form->handleRequest($request);


        // si le formulaire a été soumis on valide
        if ($form->isSubmitted()){
            // si les donnes sont valides,
            if ($form->isValid()){
                //on reccup les donnes
                $post = $form->getData();


                // on enregistre en BDD
                    // 1. On reccup le Manager Doctrine
                    $manager = $this->getDoctrine()->getManager();
                    // 2. On dit au Manager qu'il doit "gérer/surveiller" une nouvelle instance d'une Entité
                    $manager->persist($post);
                    // 3. On dit au Manager de mettre la BDD à jour
                    $manager->flush();

                    // si c'est OK, redirige
                    return $this->redirectToRoute('post');
            }
        }

        // affiche le formulaire dans la page
        return $this->render('/formulaire.html.twig', [
            'postForm' => $form->createView()
        ]);
    }

    /**
     * @Route ("/formulaire-update/{id}", name="formulaire-update")
     */
    public function update (Request $request, $id){
        // On reccupère d'abord le post en question dans le repo
        $repo = $this->getDoctrine()->getRepository(Post::class);
        $post = $repo->find([
            "id" => $id
        ]);

        // si le post demandé n'est pas trouvé (throw, c'est comme un return, ça coupe la fonction)
        if (empty($post)){
            throw $this->createNotFoundException("Le post #$id n'existe pas");
        }

        // création du formulaire pour l'instance
        $form = $this->createForm(PostType::class, $post);

        // Cette fonction sert à réccupérer la requête lorsqu'on valide le formulaire, il faut donc l'associer avec le param de la fonction create plus haut.
        $form->handleRequest($request);

        // si le formulaire a été soumis on valide
        if ($form->isSubmitted()){
            // si les donnes sont valides,
            if ($form->isValid()){
                //on reccup les donnes
                $post = $form->getData();

                // on enregistre en BDD
                // 1. On reccup le Manager Doctrine
                $manager = $this->getDoctrine()->getManager();
                // 2. On dit au Manager qu'il doit "gérer/surveiller" une nouvelle instance d'une Entité
                $manager->persist($post); //ici le persist n'est pas forcément utile par rapport à la fonction create, car elle, elle créé un nouveau post, donc php/doctrine ne le connait pas encore
                // 3. On dit au Manager de mettre la BDD à jour
                $manager->flush();

                return $this->redirectToRoute('post');
            }
        }

        // affiche le formulaire dans la page et informe twig qu'il doit associer l'id du param get avec la var php
        return $this->render('/formulaire-update.html.twig', [
            'id'=> $id,
            'postForm' => $form->createView()
        ]);
    }

    /**
     * @Route ("/comment/{id}", name="app_comment")
     * @param $id
     * @param Request $request
     * @return Response
     */
    public function comment($id, Request $request): Response
    {
        $repo = $this->getDoctrine()->getRepository(Post::class);
        $post = $repo->findOneBy([
            'id'=>$id
        ]);
        $comments = $this->getDoctrine()->getRepository(Post::class)->findBy(['parent'=>$id]);


        $newPost = new Post();

        $form = $this->createForm(PostType::class, $newPost);
        $user = $this->getUser();
        $newPost->setAuthor($user);
        $newPost->setParent($post);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $newPost = $form->getData();
            $manager = $this->getDoctrine()->getManager();

            $manager->persist($newPost);
            $manager->flush();

            return $this->redirectToRoute('app_comment', [
                'id'=>$id
            ]);
        }

        return $this->render("/comment.html.twig", [
            'id'=>$id,
            'post'=>$post,
            'comments'=>$comments,
            'formComment'=>$form->createView()
        ]);
    }


    /**
     * @Route ("/like/{id}", name="app_like")
     * @return Response
     */
    public function like($id){

        $repo = $this->getDoctrine()->getRepository(Post::class);
        /** @var Post $post */
        $post = $repo->findOneBy([
            'id'=>$id
        ]);

        /** @var User $user */
        $user = $this->getUser();

        if ($post->getLovers()->contains($user)){
            $post->removeLover($user);
        } else {
            $post->addLover($user);
        }

        $manager = $this->getDoctrine()->getManager();
        $manager->flush();

        return $this->redirectToRoute('post', [
            '_fragment'=>"$id",

        ]);
    }

    /**
     * @Route("/follow/{id}/{user}", name="app_follow")
     * @param $id
     * @param $user
     * @return Response
     */
    public function followus($id, $user):Response{

        $userProfil = $this->getDoctrine()->getRepository(User::class)->findOneBy([
            'id'=>$id,
        ]);

        $userPost = $this->getDoctrine()->getRepository(User::class)->findOneBy([
            "username" => $user
        ]);

        if (empty($userProfil)){
            throw $this->createNotFoundException("Le post #$id n'existe pas");
        }

//        if (empty($userPost)){
//            throw $this->createNotFoundException("Le post #$id n'existe pas");
//        }


        $user = $this->getUser();

        if ($userProfil->getFollow()->contains($user)){
            $userProfil->removeFollow($user);
        } else {
            $userProfil->addFollow($user);
        }

        $manager = $this->getDoctrine()->getManager();
        $manager->flush();


        return $this->redirectToRoute('userprofil', [
            'user'=>$userPost
        ]);
    }


    /**
     * @Route ("/friends/{id}/{user}", name="app_friends")
     * @param $user
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function friends($user, $id):Response{
//        $userProfil = $this->getDoctrine()->getRepository(User::class)->findOneBy([
//            "id"=>$id
//        ]);
//
//        $userPost = $this->getDoctrine()->getRepository(User::class)->findOneBy([
//            'username'=>$user
//        ]);
//
//        $user = $this->getUser();
//
//        $userProfil->addFriend($user);
////        $userProfil->removeFriend($user);
//
//        $manager = $this->getDoctrine()->getManager();
//        $manager->flush();
//
//
//        return $this->redirectToRoute('userprofil', [
//            'user'=>$userPost
//        ]);
    }
}
