<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* post/postlist.html.twig */
class __TwigTemplate_f88d200c6bd1426295a36776fadd739d439e2938ddeed64bbbf61ebd18122042 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "post/postlist.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "post/postlist.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "post/postlist.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Liste des posts";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    ";
        $this->loadTemplate("menu.html.twig", "post/postlist.html.twig", 6)->display($context);
        // line 7
        echo "
    <br>
    <br>

    <div class=\"container-sm d-flex\">
        <ul class=\"list-group col-6 list-unstyled\">
            <h1>Flux d'actualité</h1>

            ";
        // line 15
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["parents"]) || array_key_exists("parents", $context) ? $context["parents"] : (function () { throw new RuntimeError('Variable "parents" does not exist.', 15, $this->source); })()));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["parent"]) {
            // line 16
            echo "                <li class=\"mb-3\">
                    <div class=\"card\">
                        <div class=\"card-header\" id=\"";
            // line 18
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["parent"], "id", [], "any", false, false, false, 18), "html", null, true);
            echo "\">
                            ";
            // line 19
            $this->loadTemplate("avatar.html.twig", "post/postlist.html.twig", 19)->display(twig_array_merge($context, ["username" => twig_get_attribute($this->env, $this->source, $context["parent"], "author", [], "any", false, false, false, 19)]));
            // line 20
            echo "                            #";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["parent"], "id", [], "any", false, false, false, 20), "html", null, true);
            echo " by <strong><a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("userprofil", ["user" => twig_get_attribute($this->env, $this->source, $context["parent"], "author", [], "any", false, false, false, 20)]), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["parent"], "author", [], "any", false, false, false, 20), "html", null, true);
            echo "</a></strong>
                        </div>
                        <div class=\"card-body\">
                            <p class=\"card-text\">
                                ";
            // line 24
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["parent"], "content", [], "any", false, false, false, 24), "html", null, true);
            echo "
                            </p>
                            <div class=\"container d-flex justify-content-end\">
                                <a class=\"btn btn-warning fas fa-comment-alt\" href=\"";
            // line 27
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_comment", ["id" => twig_get_attribute($this->env, $this->source, $context["parent"], "id", [], "any", false, false, false, 27)]), "html", null, true);
            echo "\"></a>
                                <div class=\"text-warning ms-2\">(";
            // line 28
            echo twig_escape_filter($this->env, twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["parent"], "comment", [], "any", false, false, false, 28)), "html", null, true);
            echo ")</div>
                                ";
            // line 29
            if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 29, $this->source); })()), "user", [], "any", false, false, false, 29), "username", [], "any", false, false, false, 29), twig_get_attribute($this->env, $this->source, $context["parent"], "author", [], "any", false, false, false, 29)))) {
                // line 30
                echo "                                <a href=\"";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("formulaire-update", ["id" => twig_get_attribute($this->env, $this->source, $context["parent"], "id", [], "any", false, false, false, 30)]), "html", null, true);
                echo "\" class=\"btn btn-success ms-2 btn-sm fas fa-pen \"></a>
                                ";
            }
            // line 32
            echo "                                <a data-like=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["parent"], "id", [], "any", false, false, false, 32), "html", null, true);
            echo "\" href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_like", ["id" => twig_get_attribute($this->env, $this->source, $context["parent"], "id", [], "any", false, false, false, 32)]), "html", null, true);
            echo "\" class=\"likes\">
                                    ";
            // line 33
            if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["parent"], "lovers", [], "any", false, false, false, 33), "contains", [0 => twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 33, $this->source); })()), "user", [], "any", false, false, false, 33)], "method", false, false, false, 33)) {
                // line 34
                echo "                                        <i class=\"btn btn-danger btn-sm fas fa-heart ms-2\"></i>
                                    ";
            } else {
                // line 36
                echo "                                        <i class=\"btn text-danger border-danger btn-sm fas fa-heart ms-2\"></i>
                                    ";
            }
            // line 38
            echo "                                </a>

                                <div class=\"text-danger ms-2\">";
            // line 40
            echo twig_escape_filter($this->env, twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["parent"], "lovers", [], "any", false, false, false, 40)), "html", null, true);
            echo "</div>
                            </div>
                        </div>
                    </div>
                </li>
            ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['parent'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 46
        echo "
";
        // line 54
        echo "        </ul>


        <div class=\"space col-1\"></div>

        <div class=\"contain col-4\">
            ";
        // line 60
        if ( !twig_test_empty(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 60, $this->source); })()), "user", [], "any", false, false, false, 60))) {
            // line 61
            echo "                <div class=\"card\">
                    <div class=\"card-header\">
                        <h3>Post ton message !</h3>
                    </div>
                    <div class=\"card-header\">
                        ";
            // line 66
            echo             $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["postForm"]) || array_key_exists("postForm", $context) ? $context["postForm"] : (function () { throw new RuntimeError('Variable "postForm" does not exist.', 66, $this->source); })()), 'form');
            echo "
                    </div>
                </div>
            ";
        } else {
            // line 71
            echo "            ";
        }
        // line 72
        echo "
            <br>
            <br>
            <h3>Posts les plus récents</h3>
            <ul class=\"list-group list-unstyled\">
                ";
        // line 77
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["recentPosts"]) || array_key_exists("recentPosts", $context) ? $context["recentPosts"] : (function () { throw new RuntimeError('Variable "recentPosts" does not exist.', 77, $this->source); })()));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["post"]) {
            // line 78
            echo "                    <li class=\"mb-3\">
                        <div class=\"card\">
                            <div class=\"card-header\">
                                ";
            // line 81
            $this->loadTemplate("avatar.html.twig", "post/postlist.html.twig", 81)->display(twig_array_merge($context, ["username" => twig_get_attribute($this->env, $this->source, $context["post"], "author", [], "any", false, false, false, 81)]));
            // line 82
            echo "                                #";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "id", [], "any", false, false, false, 82), "html", null, true);
            echo " by <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("userprofil", ["user" => twig_get_attribute($this->env, $this->source, $context["post"], "author", [], "any", false, false, false, 82)]), "html", null, true);
            echo "\"><strong>";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "author", [], "any", false, false, false, 82), "html", null, true);
            echo "</strong></a>
                            </div>

                            <div class=\"card-body\">
                                <p class=\"card-text\">
                                    ";
            // line 87
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "content", [], "any", false, false, false, 87), "html", null, true);
            echo "
                                </p>
                            </div>
                        </div>
                    </li>
                ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['post'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 93
        echo "            </ul>
        </div>

    </div>




";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "post/postlist.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  297 => 93,  277 => 87,  264 => 82,  262 => 81,  257 => 78,  240 => 77,  233 => 72,  230 => 71,  223 => 66,  216 => 61,  214 => 60,  206 => 54,  203 => 46,  183 => 40,  179 => 38,  175 => 36,  171 => 34,  169 => 33,  162 => 32,  156 => 30,  154 => 29,  150 => 28,  146 => 27,  140 => 24,  128 => 20,  126 => 19,  122 => 18,  118 => 16,  101 => 15,  91 => 7,  88 => 6,  78 => 5,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Liste des posts{% endblock %}

{% block body %}
    {% include \"menu.html.twig\" %}

    <br>
    <br>

    <div class=\"container-sm d-flex\">
        <ul class=\"list-group col-6 list-unstyled\">
            <h1>Flux d'actualité</h1>

            {% for parent in parents %}
                <li class=\"mb-3\">
                    <div class=\"card\">
                        <div class=\"card-header\" id=\"{{ parent.id }}\">
                            {% include \"avatar.html.twig\" with { username: parent.author} %}
                            #{{ parent.id }} by <strong><a href=\"{{ path('userprofil', {user:parent.author}) }}\">{{ parent.author }}</a></strong>
                        </div>
                        <div class=\"card-body\">
                            <p class=\"card-text\">
                                {{ parent.content }}
                            </p>
                            <div class=\"container d-flex justify-content-end\">
                                <a class=\"btn btn-warning fas fa-comment-alt\" href=\"{{ path('app_comment', {id:parent.id}) }}\"></a>
                                <div class=\"text-warning ms-2\">({{ parent.comment | length }})</div>
                                {% if app.user.username == parent.author %}
                                <a href=\"{{ path('formulaire-update', {id:parent.id}) }}\" class=\"btn btn-success ms-2 btn-sm fas fa-pen \"></a>
                                {% endif %}
                                <a data-like=\"{{ parent.id }}\" href=\"{{ path('app_like', {id:parent.id}) }}\" class=\"likes\">
                                    {% if parent.lovers.contains(app.user) %}
                                        <i class=\"btn btn-danger btn-sm fas fa-heart ms-2\"></i>
                                    {% else %}
                                        <i class=\"btn text-danger border-danger btn-sm fas fa-heart ms-2\"></i>
                                    {% endif %}
                                </a>

                                <div class=\"text-danger ms-2\">{{ parent.lovers | length }}</div>
                            </div>
                        </div>
                    </div>
                </li>
            {% endfor %}

{#            {% set off = 5 %}#}
{#            {% set currentpath = app.request.get('_route') %}#}
{#            {% set currentPath = path(app.request.attributes.get('_route'),#}
{#                app.request.attributes.get('_route_params')) %}#}
{#            <div class=\"d-flex justify-content-center\">#}
{#                <a class=\"btn btn-primary d-flex ms-2\" href=\"{{ path(app.request.get('_route'), {offset:currentPath+5}) }}\">Page suivante</a>#}
{#            </div>#}
        </ul>


        <div class=\"space col-1\"></div>

        <div class=\"contain col-4\">
            {% if app.user is not empty %}
                <div class=\"card\">
                    <div class=\"card-header\">
                        <h3>Post ton message !</h3>
                    </div>
                    <div class=\"card-header\">
                        {{ form(postForm) }}
                    </div>
                </div>
            {% else %}
{#                {% include 'login.html.twig' %}#}
            {% endif %}

            <br>
            <br>
            <h3>Posts les plus récents</h3>
            <ul class=\"list-group list-unstyled\">
                {% for post in recentPosts %}
                    <li class=\"mb-3\">
                        <div class=\"card\">
                            <div class=\"card-header\">
                                {% include \"avatar.html.twig\" with { username: post.author} %}
                                #{{ post.id }} by <a href=\"{{ path('userprofil', {user:post.author}) }}\"><strong>{{ post.author }}</strong></a>
                            </div>

                            <div class=\"card-body\">
                                <p class=\"card-text\">
                                    {{ post.content }}
                                </p>
                            </div>
                        </div>
                    </li>
                {% endfor %}
            </ul>
        </div>

    </div>




{% endblock %}
", "post/postlist.html.twig", "/home/clem/INFREP/introduction-symfo/templates/post/postlist.html.twig");
    }
}
