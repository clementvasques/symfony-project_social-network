<?php

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.

if (\class_exists(\ContainerXJlW9Ue\App_KernelDevDebugContainer::class, false)) {
    // no-op
} elseif (!include __DIR__.'/ContainerXJlW9Ue/App_KernelDevDebugContainer.php') {
    touch(__DIR__.'/ContainerXJlW9Ue.legacy');

    return;
}

if (!\class_exists(App_KernelDevDebugContainer::class, false)) {
    \class_alias(\ContainerXJlW9Ue\App_KernelDevDebugContainer::class, App_KernelDevDebugContainer::class, false);
}

return new \ContainerXJlW9Ue\App_KernelDevDebugContainer([
    'container.build_hash' => 'XJlW9Ue',
    'container.build_id' => 'd0f414b9',
    'container.build_time' => 1621586856,
], __DIR__.\DIRECTORY_SEPARATOR.'ContainerXJlW9Ue');
