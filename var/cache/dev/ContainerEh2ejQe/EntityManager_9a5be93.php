<?php

namespace ContainerEh2ejQe;
include_once \dirname(__DIR__, 4).'/vendor/doctrine/persistence/lib/Doctrine/Persistence/ObjectManager.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManagerInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManager.php';

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{
    /**
     * @var \Doctrine\ORM\EntityManager|null wrapped object, if the proxy is initialized
     */
    private $valueHolder7c69c = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer54e4d = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties844b5 = [
        
    ];

    public function getConnection()
    {
        $this->initializer54e4d && ($this->initializer54e4d->__invoke($valueHolder7c69c, $this, 'getConnection', array(), $this->initializer54e4d) || 1) && $this->valueHolder7c69c = $valueHolder7c69c;

        return $this->valueHolder7c69c->getConnection();
    }

    public function getMetadataFactory()
    {
        $this->initializer54e4d && ($this->initializer54e4d->__invoke($valueHolder7c69c, $this, 'getMetadataFactory', array(), $this->initializer54e4d) || 1) && $this->valueHolder7c69c = $valueHolder7c69c;

        return $this->valueHolder7c69c->getMetadataFactory();
    }

    public function getExpressionBuilder()
    {
        $this->initializer54e4d && ($this->initializer54e4d->__invoke($valueHolder7c69c, $this, 'getExpressionBuilder', array(), $this->initializer54e4d) || 1) && $this->valueHolder7c69c = $valueHolder7c69c;

        return $this->valueHolder7c69c->getExpressionBuilder();
    }

    public function beginTransaction()
    {
        $this->initializer54e4d && ($this->initializer54e4d->__invoke($valueHolder7c69c, $this, 'beginTransaction', array(), $this->initializer54e4d) || 1) && $this->valueHolder7c69c = $valueHolder7c69c;

        return $this->valueHolder7c69c->beginTransaction();
    }

    public function getCache()
    {
        $this->initializer54e4d && ($this->initializer54e4d->__invoke($valueHolder7c69c, $this, 'getCache', array(), $this->initializer54e4d) || 1) && $this->valueHolder7c69c = $valueHolder7c69c;

        return $this->valueHolder7c69c->getCache();
    }

    public function transactional($func)
    {
        $this->initializer54e4d && ($this->initializer54e4d->__invoke($valueHolder7c69c, $this, 'transactional', array('func' => $func), $this->initializer54e4d) || 1) && $this->valueHolder7c69c = $valueHolder7c69c;

        return $this->valueHolder7c69c->transactional($func);
    }

    public function commit()
    {
        $this->initializer54e4d && ($this->initializer54e4d->__invoke($valueHolder7c69c, $this, 'commit', array(), $this->initializer54e4d) || 1) && $this->valueHolder7c69c = $valueHolder7c69c;

        return $this->valueHolder7c69c->commit();
    }

    public function rollback()
    {
        $this->initializer54e4d && ($this->initializer54e4d->__invoke($valueHolder7c69c, $this, 'rollback', array(), $this->initializer54e4d) || 1) && $this->valueHolder7c69c = $valueHolder7c69c;

        return $this->valueHolder7c69c->rollback();
    }

    public function getClassMetadata($className)
    {
        $this->initializer54e4d && ($this->initializer54e4d->__invoke($valueHolder7c69c, $this, 'getClassMetadata', array('className' => $className), $this->initializer54e4d) || 1) && $this->valueHolder7c69c = $valueHolder7c69c;

        return $this->valueHolder7c69c->getClassMetadata($className);
    }

    public function createQuery($dql = '')
    {
        $this->initializer54e4d && ($this->initializer54e4d->__invoke($valueHolder7c69c, $this, 'createQuery', array('dql' => $dql), $this->initializer54e4d) || 1) && $this->valueHolder7c69c = $valueHolder7c69c;

        return $this->valueHolder7c69c->createQuery($dql);
    }

    public function createNamedQuery($name)
    {
        $this->initializer54e4d && ($this->initializer54e4d->__invoke($valueHolder7c69c, $this, 'createNamedQuery', array('name' => $name), $this->initializer54e4d) || 1) && $this->valueHolder7c69c = $valueHolder7c69c;

        return $this->valueHolder7c69c->createNamedQuery($name);
    }

    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializer54e4d && ($this->initializer54e4d->__invoke($valueHolder7c69c, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializer54e4d) || 1) && $this->valueHolder7c69c = $valueHolder7c69c;

        return $this->valueHolder7c69c->createNativeQuery($sql, $rsm);
    }

    public function createNamedNativeQuery($name)
    {
        $this->initializer54e4d && ($this->initializer54e4d->__invoke($valueHolder7c69c, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializer54e4d) || 1) && $this->valueHolder7c69c = $valueHolder7c69c;

        return $this->valueHolder7c69c->createNamedNativeQuery($name);
    }

    public function createQueryBuilder()
    {
        $this->initializer54e4d && ($this->initializer54e4d->__invoke($valueHolder7c69c, $this, 'createQueryBuilder', array(), $this->initializer54e4d) || 1) && $this->valueHolder7c69c = $valueHolder7c69c;

        return $this->valueHolder7c69c->createQueryBuilder();
    }

    public function flush($entity = null)
    {
        $this->initializer54e4d && ($this->initializer54e4d->__invoke($valueHolder7c69c, $this, 'flush', array('entity' => $entity), $this->initializer54e4d) || 1) && $this->valueHolder7c69c = $valueHolder7c69c;

        return $this->valueHolder7c69c->flush($entity);
    }

    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializer54e4d && ($this->initializer54e4d->__invoke($valueHolder7c69c, $this, 'find', array('className' => $className, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer54e4d) || 1) && $this->valueHolder7c69c = $valueHolder7c69c;

        return $this->valueHolder7c69c->find($className, $id, $lockMode, $lockVersion);
    }

    public function getReference($entityName, $id)
    {
        $this->initializer54e4d && ($this->initializer54e4d->__invoke($valueHolder7c69c, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializer54e4d) || 1) && $this->valueHolder7c69c = $valueHolder7c69c;

        return $this->valueHolder7c69c->getReference($entityName, $id);
    }

    public function getPartialReference($entityName, $identifier)
    {
        $this->initializer54e4d && ($this->initializer54e4d->__invoke($valueHolder7c69c, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializer54e4d) || 1) && $this->valueHolder7c69c = $valueHolder7c69c;

        return $this->valueHolder7c69c->getPartialReference($entityName, $identifier);
    }

    public function clear($entityName = null)
    {
        $this->initializer54e4d && ($this->initializer54e4d->__invoke($valueHolder7c69c, $this, 'clear', array('entityName' => $entityName), $this->initializer54e4d) || 1) && $this->valueHolder7c69c = $valueHolder7c69c;

        return $this->valueHolder7c69c->clear($entityName);
    }

    public function close()
    {
        $this->initializer54e4d && ($this->initializer54e4d->__invoke($valueHolder7c69c, $this, 'close', array(), $this->initializer54e4d) || 1) && $this->valueHolder7c69c = $valueHolder7c69c;

        return $this->valueHolder7c69c->close();
    }

    public function persist($entity)
    {
        $this->initializer54e4d && ($this->initializer54e4d->__invoke($valueHolder7c69c, $this, 'persist', array('entity' => $entity), $this->initializer54e4d) || 1) && $this->valueHolder7c69c = $valueHolder7c69c;

        return $this->valueHolder7c69c->persist($entity);
    }

    public function remove($entity)
    {
        $this->initializer54e4d && ($this->initializer54e4d->__invoke($valueHolder7c69c, $this, 'remove', array('entity' => $entity), $this->initializer54e4d) || 1) && $this->valueHolder7c69c = $valueHolder7c69c;

        return $this->valueHolder7c69c->remove($entity);
    }

    public function refresh($entity)
    {
        $this->initializer54e4d && ($this->initializer54e4d->__invoke($valueHolder7c69c, $this, 'refresh', array('entity' => $entity), $this->initializer54e4d) || 1) && $this->valueHolder7c69c = $valueHolder7c69c;

        return $this->valueHolder7c69c->refresh($entity);
    }

    public function detach($entity)
    {
        $this->initializer54e4d && ($this->initializer54e4d->__invoke($valueHolder7c69c, $this, 'detach', array('entity' => $entity), $this->initializer54e4d) || 1) && $this->valueHolder7c69c = $valueHolder7c69c;

        return $this->valueHolder7c69c->detach($entity);
    }

    public function merge($entity)
    {
        $this->initializer54e4d && ($this->initializer54e4d->__invoke($valueHolder7c69c, $this, 'merge', array('entity' => $entity), $this->initializer54e4d) || 1) && $this->valueHolder7c69c = $valueHolder7c69c;

        return $this->valueHolder7c69c->merge($entity);
    }

    public function copy($entity, $deep = false)
    {
        $this->initializer54e4d && ($this->initializer54e4d->__invoke($valueHolder7c69c, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializer54e4d) || 1) && $this->valueHolder7c69c = $valueHolder7c69c;

        return $this->valueHolder7c69c->copy($entity, $deep);
    }

    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializer54e4d && ($this->initializer54e4d->__invoke($valueHolder7c69c, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer54e4d) || 1) && $this->valueHolder7c69c = $valueHolder7c69c;

        return $this->valueHolder7c69c->lock($entity, $lockMode, $lockVersion);
    }

    public function getRepository($entityName)
    {
        $this->initializer54e4d && ($this->initializer54e4d->__invoke($valueHolder7c69c, $this, 'getRepository', array('entityName' => $entityName), $this->initializer54e4d) || 1) && $this->valueHolder7c69c = $valueHolder7c69c;

        return $this->valueHolder7c69c->getRepository($entityName);
    }

    public function contains($entity)
    {
        $this->initializer54e4d && ($this->initializer54e4d->__invoke($valueHolder7c69c, $this, 'contains', array('entity' => $entity), $this->initializer54e4d) || 1) && $this->valueHolder7c69c = $valueHolder7c69c;

        return $this->valueHolder7c69c->contains($entity);
    }

    public function getEventManager()
    {
        $this->initializer54e4d && ($this->initializer54e4d->__invoke($valueHolder7c69c, $this, 'getEventManager', array(), $this->initializer54e4d) || 1) && $this->valueHolder7c69c = $valueHolder7c69c;

        return $this->valueHolder7c69c->getEventManager();
    }

    public function getConfiguration()
    {
        $this->initializer54e4d && ($this->initializer54e4d->__invoke($valueHolder7c69c, $this, 'getConfiguration', array(), $this->initializer54e4d) || 1) && $this->valueHolder7c69c = $valueHolder7c69c;

        return $this->valueHolder7c69c->getConfiguration();
    }

    public function isOpen()
    {
        $this->initializer54e4d && ($this->initializer54e4d->__invoke($valueHolder7c69c, $this, 'isOpen', array(), $this->initializer54e4d) || 1) && $this->valueHolder7c69c = $valueHolder7c69c;

        return $this->valueHolder7c69c->isOpen();
    }

    public function getUnitOfWork()
    {
        $this->initializer54e4d && ($this->initializer54e4d->__invoke($valueHolder7c69c, $this, 'getUnitOfWork', array(), $this->initializer54e4d) || 1) && $this->valueHolder7c69c = $valueHolder7c69c;

        return $this->valueHolder7c69c->getUnitOfWork();
    }

    public function getHydrator($hydrationMode)
    {
        $this->initializer54e4d && ($this->initializer54e4d->__invoke($valueHolder7c69c, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializer54e4d) || 1) && $this->valueHolder7c69c = $valueHolder7c69c;

        return $this->valueHolder7c69c->getHydrator($hydrationMode);
    }

    public function newHydrator($hydrationMode)
    {
        $this->initializer54e4d && ($this->initializer54e4d->__invoke($valueHolder7c69c, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializer54e4d) || 1) && $this->valueHolder7c69c = $valueHolder7c69c;

        return $this->valueHolder7c69c->newHydrator($hydrationMode);
    }

    public function getProxyFactory()
    {
        $this->initializer54e4d && ($this->initializer54e4d->__invoke($valueHolder7c69c, $this, 'getProxyFactory', array(), $this->initializer54e4d) || 1) && $this->valueHolder7c69c = $valueHolder7c69c;

        return $this->valueHolder7c69c->getProxyFactory();
    }

    public function initializeObject($obj)
    {
        $this->initializer54e4d && ($this->initializer54e4d->__invoke($valueHolder7c69c, $this, 'initializeObject', array('obj' => $obj), $this->initializer54e4d) || 1) && $this->valueHolder7c69c = $valueHolder7c69c;

        return $this->valueHolder7c69c->initializeObject($obj);
    }

    public function getFilters()
    {
        $this->initializer54e4d && ($this->initializer54e4d->__invoke($valueHolder7c69c, $this, 'getFilters', array(), $this->initializer54e4d) || 1) && $this->valueHolder7c69c = $valueHolder7c69c;

        return $this->valueHolder7c69c->getFilters();
    }

    public function isFiltersStateClean()
    {
        $this->initializer54e4d && ($this->initializer54e4d->__invoke($valueHolder7c69c, $this, 'isFiltersStateClean', array(), $this->initializer54e4d) || 1) && $this->valueHolder7c69c = $valueHolder7c69c;

        return $this->valueHolder7c69c->isFiltersStateClean();
    }

    public function hasFilters()
    {
        $this->initializer54e4d && ($this->initializer54e4d->__invoke($valueHolder7c69c, $this, 'hasFilters', array(), $this->initializer54e4d) || 1) && $this->valueHolder7c69c = $valueHolder7c69c;

        return $this->valueHolder7c69c->hasFilters();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);

        $instance->initializer54e4d = $initializer;

        return $instance;
    }

    protected function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, \Doctrine\Common\EventManager $eventManager)
    {
        static $reflection;

        if (! $this->valueHolder7c69c) {
            $reflection = $reflection ?? new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHolder7c69c = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);

        }

        $this->valueHolder7c69c->__construct($conn, $config, $eventManager);
    }

    public function & __get($name)
    {
        $this->initializer54e4d && ($this->initializer54e4d->__invoke($valueHolder7c69c, $this, '__get', ['name' => $name], $this->initializer54e4d) || 1) && $this->valueHolder7c69c = $valueHolder7c69c;

        if (isset(self::$publicProperties844b5[$name])) {
            return $this->valueHolder7c69c->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder7c69c;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder7c69c;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializer54e4d && ($this->initializer54e4d->__invoke($valueHolder7c69c, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer54e4d) || 1) && $this->valueHolder7c69c = $valueHolder7c69c;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder7c69c;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder7c69c;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializer54e4d && ($this->initializer54e4d->__invoke($valueHolder7c69c, $this, '__isset', array('name' => $name), $this->initializer54e4d) || 1) && $this->valueHolder7c69c = $valueHolder7c69c;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder7c69c;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHolder7c69c;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializer54e4d && ($this->initializer54e4d->__invoke($valueHolder7c69c, $this, '__unset', array('name' => $name), $this->initializer54e4d) || 1) && $this->valueHolder7c69c = $valueHolder7c69c;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder7c69c;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHolder7c69c;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializer54e4d && ($this->initializer54e4d->__invoke($valueHolder7c69c, $this, '__clone', array(), $this->initializer54e4d) || 1) && $this->valueHolder7c69c = $valueHolder7c69c;

        $this->valueHolder7c69c = clone $this->valueHolder7c69c;
    }

    public function __sleep()
    {
        $this->initializer54e4d && ($this->initializer54e4d->__invoke($valueHolder7c69c, $this, '__sleep', array(), $this->initializer54e4d) || 1) && $this->valueHolder7c69c = $valueHolder7c69c;

        return array('valueHolder7c69c');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer54e4d = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer54e4d;
    }

    public function initializeProxy() : bool
    {
        return $this->initializer54e4d && ($this->initializer54e4d->__invoke($valueHolder7c69c, $this, 'initializeProxy', array(), $this->initializer54e4d) || 1) && $this->valueHolder7c69c = $valueHolder7c69c;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder7c69c;
    }

    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder7c69c;
    }
}

if (!\class_exists('EntityManager_9a5be93', false)) {
    \class_alias(__NAMESPACE__.'\\EntityManager_9a5be93', 'EntityManager_9a5be93', false);
}
